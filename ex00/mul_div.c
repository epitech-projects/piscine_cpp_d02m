/*
** mul_div.c for Add Mul in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex00
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Thu Jan  9 10:47:19 2014 Jean Gravier
** Last update Thu Jan  9 11:15:09 2014 Jean Gravier
*/

void	add_mul_4param(int first, int second, int *add, int *mul)
{
  *add = first + second;
  *mul = first * second;
}

void	add_mul_2param(int *first, int *second)
{
  int	temp_first;

  temp_first = *first;
  *first += *second;
  *second *= temp_first;
}
