/*
** tab_to_2dtab.c for 2dtab in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex02
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Thu Jan  9 12:08:03 2014 Jean Gravier
** Last update Fri Jan 10 09:09:28 2014 Jean Gravier
*/

#include <stdlib.h>
#include <stdio.h>

void	tab_to_2dtab(int *tab, int length, int width, int ***res)
{
  int	x;
  int	y;

  y = 0;
  *res = malloc(sizeof(**res) * length);
  while (y < length)
    {
      (*res)[y] = malloc(sizeof(***res) * width);
      while (x < width)
	{
	  (*res)[y][x] = tab[(y * width) + x];
	  ++x;
	}
      x = 0;
      ++y;
    }
}
