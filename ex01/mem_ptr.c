/*
** mem_ptr.c for Mem Ptr in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex01
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Thu Jan  9 11:21:33 2014 Jean Gravier
** Last update Thu Jan  9 12:02:31 2014 Jean Gravier
*/

#include <stdlib.h>
#include <string.h>
#include "mem_ptr.h"

void	add_str(char *str1, char *str2, char **res)
{
  *res = malloc(sizeof(*res) * strlen(str1) + strlen(str2));
  *res = strcat(*res, str1);
  *res = strcat(*res, str2);
}

void	add_str_struct(t_str_op *str_op)
{
  str_op->res = malloc(strlen(str_op->str1) + strlen(str_op->str2));

  str_op->res = strcat(str_op->res, str_op->str1);
  str_op->res = strcat(str_op->res, str_op->str2);
}
