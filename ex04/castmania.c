/*
** castmania.c for Cast Mania in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex04
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Fri Jan 10 02:22:20 2014 Jean Gravier
** Last update Fri Jan 10 09:59:02 2014 Jean Gravier
*/

#include "castmania.h"
#include <stdio.h>

void	exec_operation(t_instruction_type instruction_type, void *data)
{
  void	*temp;

  if (instruction_type == ADD_OPERATION)
    {
      exec_add(((t_instruction *)data)->operation);
      if (((t_instruction *)data)->output_type == VERBOSE)
	{
	  temp = &(((t_add *)((t_instruction *)data)->operation)->add_op);
	  printf("%d\n", ((t_integer_op *)temp)->res);
	}
    }
  else if (instruction_type == DIV_OPERATION)
    {
      exec_div(((t_instruction *)data)->operation);
      if ((t_div_type)(((t_add *)((t_instruction *)data)->operation)->add_type) == INTEGER)
	{
	  printf("%d\n", ((t_integer_op *)((t_div *)((t_instruction *)data)->operation)->div_op)->res);
	}
      else if ((t_div_type)(((t_add *)((t_instruction *)data)->operation)->add_type) == DECIMALE)
	{
	  printf("%f\n", ((t_decimale_op *)((t_div *)((t_instruction *)data)->operation)->div_op)->res);
	}
    }
}

void	exec_instruction(t_instruction_type instruction_type, void *data)
{
  int	*i_temp;
  float	*f_temp;

  if (instruction_type == PRINT_INT)
    {
      i_temp = (int *)data;
      printf("%d\n", *i_temp);
    }
  else if (instruction_type == PRINT_FLOAT)
    {
      f_temp = (float *)data;
      printf("%f\n", *f_temp);
    }
}
