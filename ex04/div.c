/*
** div.c for Cast Mania in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex04
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Thu Jan  9 23:37:59 2014 Jean Gravier
** Last update Fri Jan 10 09:37:40 2014 Jean Gravier
*/

#include "castmania.h"
#include <stdio.h>

int	integer_div(int a, int b)
{
  if (!b)
    {
      return (0);
    }
  return (a / b);
}

float	decimale_div(int a, int b)
{
  if (!b)
    {
      return (0);
    }
  return ((float)a / (float)b);
}

void	exec_div(t_div *operation)
{
  void	*temp;
  
  temp = (operation->div_op);

  if (operation->div_type == INTEGER)
    {
      ((t_integer_op *)temp)->res = integer_div(((t_integer_op *)temp)->a, ((t_integer_op *)temp)->b);
    }
  else if (operation->div_type == DECIMALE)
    {
      ((t_integer_op *)temp)->res = decimale_div(((t_integer_op *)temp)->a, ((t_integer_op *)temp)->b);
    }
}
