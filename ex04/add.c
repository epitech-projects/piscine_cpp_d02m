/*
** add.c for Cast Mania in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex04
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Fri Jan 10 02:14:36 2014 Jean Gravier
** Last update Fri Jan 10 09:44:17 2014 Jean Gravier
*/

#include "castmania.h"
#include <stdio.h>

int	normal_add(int a, int b)
{
  return (a + b);
}


int	absolute_add(int a, int b)
{
  if (a < 0)
    a *= -1;
  if (b < 0)
    b *= -1;
  
  return (a + b);
}

void	exec_add(t_add *operation)
{
  t_integer_op	*temp;
  
  temp = &(operation->add_op);

  if (operation->add_type == NORMAL)
    temp->res = normal_add(temp->a, temp->b);
  else if (operation->add_type == ABSOLUTE)
    temp->res = absolute_add(temp->a, temp->b);
}
