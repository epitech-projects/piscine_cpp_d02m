/*
** func_ptr.h for Func Ptr in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex03
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Thu Jan  9 18:56:38 2014 Jean Gravier
** Last update Fri Jan 10 00:18:44 2014 Jean Gravier
*/

#ifndef FUNC_PTR_H_
# define FUNC_PTR_H_

# include "func_ptr_enum.h"

void	print_normal(char *str);
void	print_reverse(char *str);
void	print_upper(char *str);
void	print_42(char *str);
void	do_action(t_action action, char *str);

#endif /* FUNC_PTR_H_ */
