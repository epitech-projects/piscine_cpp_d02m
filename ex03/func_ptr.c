/*
** func_ptr.c for Func Ptr in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d02m/ex03
** 
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
** 
** Started on  Thu Jan  9 18:33:06 2014 Jean Gravier
** Last update Fri Jan 10 00:40:13 2014 Jean Gravier
*/

#include <unistd.h>
#include <string.h>
#include "func_ptr.h"

void	print_normal(char *str)
{
  write(1, str, strlen(str));
  write(1, "\n", 1);
}

void	print_reverse(char *str)
{
  int	i;

  i = strlen(str) - 1;
  while (i >= 0)
    {
      write(1, &str[i--], 1);
    }
  write(1, "\n", 1);
}

void	print_upper(char *str)
{
  int	i;
  int	c;

  i = 0;
  while (str[i])
    {      
      if (str[i] >= 97 && str[i] <= 122)
	{
	  c = str[i] - 32;
	  write(1, &c, 1);
	}
      else
	{
	  write(1, &str[i], 1);
	}
      i++;
    }
  write(1, "\n", 1);
}

void	print_42(char *str)
{
  (void)str;
  write(1, "42\n", 3);
}

void	do_action(t_action action, char *str)
{
  void (*func_ptr[4])(char *);

  func_ptr[0] = print_normal;
  func_ptr[1] = print_reverse;
  func_ptr[2] = print_upper;
  func_ptr[3] = print_42;
  (*func_ptr[action])(str);
}
